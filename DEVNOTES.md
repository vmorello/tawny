## PSRFITS Documentation

https://www.atnf.csiro.au/research/pulsar/psrfits_definition/PsrfitsDocumentation.html  

Search-mode data may be stored as 1-bit, 2-bit, 4-bit or 8-bit signed or unsigned integers and are written as a byte array. Data digitised with less than 8 bits are packed with earlier samples in higher-order bits of the byte (i.e., "big-endian"). Elements of the data array are in channel, polarisation and sample order **with the spectral channels in contiguous locations.** (emphasis mine). In C-ordering `numpy` parlance, this means a three-dimensional shape `(nsamp // samples_per_byte, npol, nchan)`.

**The data in FITS files is always stored in "big-endian" byte order**  
Source: https://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/f_user/node31.html

**The size of a FITS file must be a multiple of 2880 bytes**. This may require padding the end of the file with the ASCII character 00 up to a multiple of 2880 bytes. Look up "FITS block size":
http://www.eso.org/sci/software/esomidas/doc/user/18NOV/vola/node111.html


## dspsr remarks

The version of dspsr currently installed on OzStar applied the PSRFITS offsets and scales when folding. The version installed on my desktop does **not** apply offsets and scales (this is probably just for search mode data).


## Channel ordering

For both prepsubband and psrchive/dspsr, channel index 0 is the lowest frequency channel. No need to worry about flipping the channel masks.


## Header Layout

This is based on a UWL search mode observation taken in September 2020. The FITS contains three so-called HDUs (Header Data Unit) in the following order.


#### Primary HDU header

```
SIMPLE  =                    T / file does conform to FITS standard             
BITPIX  =                    8 / number of bits per data pixel                  
NAXIS   =                    0 / number of data axes                            
EXTEND  =                    T / FITS dataset may contain extensions            
COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy
COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H 
COMMENT   FITS (Flexible Image Transport System) format defined in Astronomy and
COMMENT   Astrophysics Supplement Series v44/p363, v44/p371, v73/p359, v73/p365.
COMMENT   Contact the NASA Science Office of Standards and Technology for the   
COMMENT   FITS Definition document #100 and other FITS information.             
HDRVER  = '6.2             '   / Header version                                 
FITSTYPE= 'PSRFITS         '   / FITS definition for pulsar data files          
DATE    = '2020-10-02T07:06:24' / File creation date (YYYY-MM-DDThh:mm:ss UTC)  
OBSERVER= 'kea095  '           / Observer name(s)                               
PROJID  = 'P1082   '           / Project name                                   
TELESCOP= 'Parkes  '           / Telescope name                                 
ANT_X   =           -4554231.6 / [m] Antenna ITRF X-coordinate (D)              
ANT_Y   =            2816759.1 / [m] Antenna ITRF Y-coordinate (D)              
ANT_Z   =           -3454036.1 / [m] Antenna ITRF Z-coordinate (D)              
FRONTEND= 'UWL     '           / Receiver ID                                    
IBEAM   =                    1 / Beam ID for multibeam systems                  
NRCVR   =                    2 / Number of receiver polarisation channels       
FD_POLN = 'LIN     '           / LIN or CIRC                                    
FD_HAND =                   -1 / +/- 1. +1 is LIN:A=X,B=Y, CIRC:A=L,B=R (I)     
FD_SANG =                    0 / [deg] FA of E vect for equal sig in A&B (E)    
FD_XYPH =                   0. / [deg] Phase of A^* B for injected cal (E)      
BACKEND = 'Medusa  '           / Backend ID                                     
BECONFIG= 'Medusa  '           / Backend configuration file name                
BE_PHASE=                    1 / 0/+1/-1 BE cross-phase:0 unknown,+/-1 std/rev  
BE_DCC  =                    1 / 0/1 BE downconversion conjugation corrected    
BE_DELAY=                   0. / [s] Backend propn delay from digitiser input   
TCYCLE  =                    0 / [s] On-line cycle time (D)                     
OBS_MODE= 'SEARCH  '           / (PSR, CAL, SEARCH)                             
DATE-OBS= '2020-10-01T03:55:33' / Date of observation (YYYY-MM-DDThh:mm:ss UTC) 
OBSFREQ =                2368. / [MHz] Centre frequency for observation         
OBSBW   =                 3328 / [MHz] Bandwidth for observation                
OBSNCHAN=                26624 / Number of frequency channels (original)        
CHAN_DM =                  650 / [cm-3 pc] DM used for on-line dedispersion     
PNT_ID  = '        '           / Name or ID for pointing ctr (multibeam feeds)  
SRC_NAME= 'Mercer_5'           / Source or scan ID                              
COORD_MD= 'J2000   '           / Coordinate mode (J2000, GALACTIC, ECLIPTIC)    
EQUINOX =                2000. / Equinox of coords (e.g. 2000.0)                
RA      = '18:23:19.000'       / Right ascension (hh:mm:ss.ssss)                
DEC     = '-13:40:02.000'      / Declination (-dd:mm:ss.sss)                    
BMAJ    =                    0 / [deg] Beam major axis length                   
BMIN    =                    0 / [deg] Beam minor axis length                   
BPA     =                    0 / [deg] Beam position angle                      
STT_CRD1= 'UNSET   '           / Start coord 1 (hh:mm:ss.sss or ddd.ddd)        
STT_CRD2= 'UNSET   '           / Start coord 2 (-dd:mm:ss.sss or -dd.ddd)       
TRK_MODE= 'TRACK   '           / Track mode (TRACK, SCANGC, SCANLAT)            
STP_CRD1= '18:23:19'           / Stop coord 1 (hh:mm:ss.sss or ddd.ddd)         
STP_CRD2= '-13:40:02'          / Stop coord 2 (-dd:mm:ss.sss or -dd.ddd)        
SCANLEN =                28800 / [s] Requested scan length (E)                  
FD_MODE = 'FA      '           / Feed track mode - FA, CPA, SPA, TPA            
FA_REQ  =                   0. / [deg] Feed/Posn angle requested (E)            
CAL_MODE= 'OFF     '           / Cal mode (OFF, SYNC, EXT1, EXT2)               
CAL_FREQ= '*       '           / [Hz] Cal modulation frequency (E)              
CAL_DCYC= '*       '           / Cal duty cycle (E)                             
CAL_PHS = '*       '           / Cal phase (wrt start time) (E)                 
CAL_NPHS= '*       '           / Number of states in cal pulse (I)              
STT_IMJD=                59123 / Start MJD (UTC days) (J - long integer)        
STT_SMJD=                14133 / [s] Start time (sec past UTC 00h) (J)          
STT_OFFS=             0.101936 / [s] Start time offset (D)                      
STT_LST =                   0. / [s] Start LST (D)              
```


#### Secondary HDU header


```
XTENSION= 'BINTABLE'           / ***** Processing history *****                 
BITPIX  =                    8 / N/A                                            
NAXIS   =                    2 / 2-dimensional binary table                     
NAXIS1  =                  820 / width of table in bytes                        
NAXIS2  =                    1 / number of rows                                 
PCOUNT  =                    0 / size of special data area                      
GCOUNT  =                    1 / one data group (required keyword)              
TFIELDS =                   29 / number of fields per row                       
TTYPE1  = 'DATE_PRO'           / Processing date and time (UTC)                 
TFORM1  = '24A     '           / 24-char string                                 
TTYPE2  = 'PROC_CMD'           / Processing program and command                 
TFORM2  = '256A    '           / 256-char string                                
TTYPE3  = 'SCALE   '           / Units (FluxDen/RefFlux/Jansky)                 
TFORM3  = '8A      '           / 8-char string                                  
TTYPE4  = 'POL_TYPE'           / Polarisation identifier                        
TFORM4  = '8A      '           / 8-char string                                  
TTYPE5  = 'NSUB    '           / Number of Sub-Integrations                     
TFORM5  = '1J      '           / Long integer                                   
TTYPE6  = 'NPOL    '           / Number of polarisations                        
TFORM6  = '1I      '           / Integer                                        
TTYPE7  = 'NBIN    '           / Nr of bins per product (0 for SEARCH mode)     
TFORM7  = '1I      '           / Integer                                        
TTYPE8  = 'NBIN_PRD'           / Nr of bins per period                          
TFORM8  = '1I      '           / Integer                                        
TTYPE9  = 'TBIN    '           / Time per bin or sample                         
TFORM9  = '1D      '           / Double                                         
TTYPE10 = 'CTR_FREQ'           / Band centre frequency (weighted)               
TFORM10 = '1D      '           / Double                                         
TTYPE11 = 'NCHAN   '           / Number of frequency channels                   
TFORM11 = '1J      '           / Long integer                                   
TTYPE12 = 'CHAN_BW '           / Channel bandwidth                              
TFORM12 = '1D      '           / Double                                         
TTYPE13 = 'REF_FREQ'           / Reference frequency                            
TFORM13 = '1D      '           / Double                                         
TTYPE14 = 'DM      '           / DM used for dedispersion                       
TFORM14 = '1D      '           / Double                                         
TTYPE15 = 'RM      '           / RM used for RM correction                      
TFORM15 = '1D      '           / Double                                         
TTYPE16 = 'PR_CORR '           / Projection of receptors onto sky corrected     
TFORM16 = '1I      '           / Integer flag                                   
TTYPE17 = 'FD_CORR '           / Feed basis correction applied                  
TFORM17 = '1I      '           / Integer flag                                   
TTYPE18 = 'BE_CORR '           / Backend correction applied                     
TFORM18 = '1I      '           / Integer flag                                   
TTYPE19 = 'RM_CORR '           / RM correction applied                          
TFORM19 = '1I      '           / Integer flag                                   
TTYPE20 = 'DEDISP  '           / Data dedispersed                               
TFORM20 = '1I      '           / Integer flag                                   
TTYPE21 = 'DDS_MTHD'           / Dedispersion method                            
TFORM21 = '32A     '           / 32-char string                                 
TTYPE22 = 'SC_MTHD '           / Scattered power correction method              
TFORM22 = '32A     '           / 32-char string                                 
TTYPE23 = 'CAL_MTHD'           / Calibration method                             
TFORM23 = '32A     '           / 32-char string                                 
TTYPE24 = 'CAL_FILE'           / Name of gain calibration file                  
TFORM24 = '256A    '           / 256-char string                                
TTYPE25 = 'RFI_MTHD'           / RFI excision method                            
TFORM25 = '32A     '           / 32-char string                                 
TTYPE26 = 'RM_MODEL'           / Auxiliary Faraday rotation model description   
TFORM26 = '32A     '           / 32-char string                                 
TTYPE27 = 'AUX_RM_C'           / Auxiliary Faraday rotation corrected flag      
TFORM27 = '1I      '           / Integer flag                                   
TTYPE28 = 'DM_MODEL'           / Auxiliary dispersion model description         
TFORM28 = '32A     '           / 32-char string                                 
TTYPE29 = 'AUX_DM_C'           / Auxiliary dispersion corrected flag            
TFORM29 = '1I      '           / Integer flag                                   
EXTNAME = 'HISTORY '           / name of this binary table extension            
TUNIT9  = 's       '           / units of field                                 
TUNIT10 = 'MHz     '           / units of field                                 
TUNIT12 = 'MHz     '           / units of field                                 
TUNIT13 = 'MHz     '           / units of field                                 
TUNIT14 = 'CM-3    '                                                            
TUNIT15 = 'RAD     '                                                            
EXTVER  =                    1 / auto assigned by template parser  
```


#### Binary Table HDU header

```
XTENSION= 'BINTABLE'           / ***** Subintegration data  *****               
BITPIX  =                    8 / N/A                                            
NAXIS   =                    2 / 2-dimensional binary table                     
NAXIS1  =             55058472 / width of table in bytes                        
NAXIS2  =                  228 / Number of rows in table (NSUBINT)              
PCOUNT  =                    0 / size of special data area                      
GCOUNT  =                    1 / one data group (required keyword)              
TFIELDS =                   10 / Number of fields per row                       
TTYPE1  = 'INDEXVAL'           / Optionally used if INT_TYPE != TIME            
TFORM1  = '1D      '           / Double                                         
TTYPE2  = 'TSUBINT '           / Length of subintegration                       
TFORM2  = '1D      '           / Double                                         
TTYPE3  = 'OFFS_SUB'           / Offset from Start of subint centre             
TFORM3  = '1D      '           / Double                                         
TTYPE4  = 'AUX_DM  '           / additional DM (ionosphere, corona, etc.)       
TFORM4  = '1D      '           / Double                                         
TTYPE5  = 'AUX_RM  '           / additional RM (ionosphere, corona, etc.)       
TFORM5  = '1D      '           / Double                                         
TTYPE6  = 'DAT_FREQ'           / [MHz] Centre frequency for each channel        
TFORM6  = '26624D  '           / NCHAN doubles                                  
TTYPE7  = 'DAT_WTS '           / Weights for each channel                       
TFORM7  = '26624E  '           / NCHAN floats                                   
TTYPE8  = 'DAT_OFFS'           / Data offset for each channel                   
TFORM8  = '26624E  '           / NCHAN*NPOL floats                              
TTYPE9  = 'DAT_SCL '           / Data scale factor (outval=dataval*scl + offs)  
TFORM9  = '26624E  '           / NCHAN*NPOL floats                              
EPOCHS  = 'VALID   '           / Epoch convention (VALID, MIDTIME, STT_MJD)     
INT_TYPE= 'TIME    '           / Time axis (TIME, BINPHSPERI, BINLNGASC, etc)   
INT_UNIT= 'SEC     '           / Unit of time axis (SEC, PHS (0-1), DEG)        
SCALE   = 'FluxDen '           / Intensity units (FluxDen/RefFlux/Jansky)       
POL_TYPE= 'AA+BB   '           / Polarisation identifier (e.g., AABBCRCI, AA+BB)
NPOL    =                    1 / Nr of polarisations                            
TBIN    =              6.4E-05 / [s] Time per bin or sample                     
NBIN    =                    1 / Nr of bins (PSR/CAL mode; else 1)              
NBIN_PRD= '*       '           / Nr of bins/pulse period (for gated data)       
PHS_OFFS= '*       '           / Phase offset of bin 0 for gated data           
NBITS   =                    2 / Nr of bits/datum (SEARCH mode data, else 1)    
ZERO_OFF=                  1.5 / Zero offset for SEARCH-mode data               
SIGNINT =                    0 / 1 for signed ints in SEARCH-mode data, else 0  
NSUBOFFS=                    0 / Subint offset (Contiguous SEARCH-mode files)   
NCHAN   =                26624 / Number of channels/sub-bands in this file      
CHAN_BW =                0.125 / [MHz] Channel/sub-band width                   
REFFREQ =                   0. / [MHz] Reference frequency                      
DM      =                 650. / [cm-3 pc] DM for post-detection dedisperion    
RM      =                   0. / [rad m-2] RM for post-detection deFaraday      
NCHNOFFS=                    0 / Channel/sub-band offset for split files        
NSBLK   =                 8192 / Samples/row (SEARCH mode, else 1)              
NSTOT   =               466944 / Total number of samples (SEARCH mode, else 1)  
EXTNAME = 'SUBINT  '           / name of this binary table extension            
TUNIT2  = 's       '           / Units of field                                 
TUNIT3  = 's       '           / Units of field                                 
TUNIT4  = 'CM-3    '                                                            
TUNIT5  = 'RAD     '                                                            
TUNIT6  = 'MHz     '           / Units of field                                 
EXTVER  =                    1 / auto assigned by template parser               
TTYPE10 = 'DATA    '           / label for field                                
TFORM10 = '54525952B'          / format of field                                
TDIM10  = '(26624,1,2048)'     / size of the multidimensional array  
```

#### Binary data layout

NOTE: From one observation to the next I have noticed some changes in the ordering of these columns. The exact ordering is specified by the fields TTYPE{N} above. One "row" corresponds to one sub-integration, where each contains NSBLK time samples. 


```
[INDEXVAL][TSUBINT][OFFS_SUB][AUX_DM][AUX_RM][DAT_FREQ][DAT_WTS][DAT_OFFS][DAT_SCL][DATA]
[INDEXVAL][TSUBINT][OFFS_SUB][AUX_DM][AUX_RM][DAT_FREQ][DAT_WTS][DAT_OFFS][DAT_SCL][DATA]
...
[INDEXVAL][TSUBINT][OFFS_SUB][AUX_DM][AUX_RM][DAT_FREQ][DAT_WTS][DAT_OFFS][DAT_SCL][DATA]
```

## Shape of the data

NOTE: read the following link about data ordering !
https://docs.astropy.org/en/stable/io/fits/#working-with-image-data

In the example above, the raw data are 2-bit packed, with 26624 channels and 8192 samples per subint. There are 228 subints in total. The raw data blocks are presented as a 3D array with shape (nsamp_packed, npol, nchan) = (2048, 1, 26624). One row thus contains four time samples according to the section above about PSRFITS.


## Extracting a byte range or a time interval from a file

To extract an arbitray byte range:

```bash
# s = start byte index
# e = end byte index
# This extracts byte range s:e (in Python convention)
tail -c +(s+1) <filename> | head -c (s-e) > <outfile>
```

To extract a time range, extract the header first

```bash
# Assuming header is 11520 bytes
head -c 11520 <filename> >> <outfile>
```

Then, edit the value of NAXIS2 with a text editor. Make sure it occupies the same number of characters as before (insert/remove spaces if needed). `NAXIS2 = floor(DESIRED_TOBS / (NSBLK * TBIN))`.  

Finally, append the data to the file. We need `nbytes = NAXIS2 * NAXIS1`.

```bash
tail -c +11521 <filename> | head -c nbytes >> <outfile>
```






## Bit unpacking

General note: in computer science, a right shift is a shift towards the least significant bits, regardless of the machine endianness.

### Original SIGPROC code

Below is the relevant snippet taken from SIGPROC's source code:
https://github.com/SixByNine/sigproc/blob/master/src/pack_unpack.c

```C
#define HI2BITS 192
#define UPMED2BITS 48
#define LOMED2BITS 12
#define LO2BITS 3

void char2fourints (unsigned char c, int *i, int *j, int *k, int *l) /* includefile */
{
  *i =  c & LO2BITS;
  *j = (c & LOMED2BITS) >> 2;
  *k = (c & UPMED2BITS) >> 4;
  *l = (c & HI2BITS) >> 6;
}
```

### Ewan Barr's sugpyproc

Below is Ewan Barr's `sigpryproc` snippet which assumes the opposite order:
https://github.com/ewanbarr/sigpyproc/blob/master/c_src/libSigPyProc.c

```C
#define HI4BITS 240
#define LO4BITS 15
#define HI2BITS 192
#define UPMED2BITS 48
#define LOMED2BITS 12
#define LO2BITS 3

/*----------------------------------------------------------------------------*/

/*Function to unpack 1,2 and 4 bit data
data is unpacked into an empty buffer
Note: Only unpacks big endian bit ordering*/
void unpack(unsigned char* inbuffer,
	    unsigned char* outbuffer,
	    int nbits,
	    int nbytes)
{
  int ii,jj;
  switch(nbits){
  case 1:
    for(ii=0;ii<nbytes;ii++){
      for(jj=0;jj<8;jj++){
	outbuffer[(ii*8)+jj] = (inbuffer[ii]>>jj)&1;
      }
    }
    break;
  case 2:
    for(ii=0;ii<nbytes;ii++){
      outbuffer[(ii*4)+3] = inbuffer[ii] & LO2BITS;
      outbuffer[(ii*4)+2] = (inbuffer[ii] & LOMED2BITS) >> 2;
      outbuffer[(ii*4)+1] = (inbuffer[ii] & UPMED2BITS) >> 4;
      outbuffer[(ii*4)+0] = (inbuffer[ii] & HI2BITS) >> 6;
    }
    break;
  case 4:
    for(ii=0;ii<nbytes;ii++){
      outbuffer[(ii*2)+1] = inbuffer[ii] & LO4BITS;
      outbuffer[(ii*2)+0] = (inbuffer[ii] & HI4BITS) >> 4;
    } 
    break;
  }
}
```
