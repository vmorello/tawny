import logging
import argparse
from tawny import PSRFITS, extract_channel_range


def get_parser():
    parser = argparse.ArgumentParser(
        description='Extract channel range from PSRFITS file and write the output to another file'
    )
    parser.add_argument('infile', type=str, help='Input PSRFITS file')
    parser.add_argument('outfile', type=str, help='Output PSRFITS file')
    parser.add_argument('-s', '--start', type=int, required=True, help='Start channel index, INclusive')
    parser.add_argument('-e', '--end', type=int, required=True, help='End channel index, EXclusive')
    parser.add_argument('--overwrite', action='store_true', help='If specified, overwrite output file')
    parser.add_argument('--alpha', type=float, default=None, 
        help='Spectral index to postulate if re-weighting the channels for dedispersion.'
            'If unspecified, do NOT change the weights.'
    )
    return parser


def main():
    logging.basicConfig(
        level='DEBUG',
        format='%(asctime)s  %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s'
    )
    args = get_parser().parse_args()
    extract_channel_range(
        args.infile, args.outfile, args.start, args.end, 
        overwrite=args.overwrite, 
        reweight=args.alpha is not None,
        spectral_index=args.alpha
    )


if __name__ == '__main__':
    main()
