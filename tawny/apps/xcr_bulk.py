import logging
import argparse
import os
from tawny import PSRFITS, extract_channel_range


def get_parser():
    parser = argparse.ArgumentParser(
        description='Extract channel range from multiple PSRFITS files. Output files are written'
            ' to the specified output directory, with the same file name.'
    )
    parser.add_argument('infiles', type=str, nargs='+', help='Input PSRFITS files')
    parser.add_argument('-o','--outdir', type=str, required=True, help='Output directory')
    parser.add_argument('-s', '--start', type=int, required=True, help='Start channel index, INclusive')
    parser.add_argument('-e', '--end', type=int, required=True, help='End channel index, EXclusive')
    parser.add_argument('--overwrite', action='store_true', help='If specified, overwrite output file')
    parser.add_argument('--alpha', type=float, default=None, 
        help='Spectral index to postulate if re-weighting the channels for dedispersion.'
            'If unspecified, do NOT change the weights.'
    )
    return parser


def main():
    logging.basicConfig(
        level='DEBUG',
        format='%(asctime)s  %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s'
    )
    args = get_parser().parse_args()

    for fname in args.infiles:
        __, basename = os.path.split(fname)
        outfile = os.path.join(args.outdir, basename)
        extract_channel_range(
            fname, outfile, args.start, args.end, 
            overwrite=args.overwrite, 
            reweight=args.alpha is not None, 
            spectral_index=args.alpha
        )


if __name__ == '__main__':
    main()