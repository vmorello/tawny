import logging
import argparse
from tawny import PSRFITS, extract_subint_range


def get_parser():
    parser = argparse.ArgumentParser(
        description="Extract sub-integration range from PSRFITS file and write the output to "
        "another file"
    )
    parser.add_argument("infile", type=str, help="Input PSRFITS file")
    parser.add_argument("outfile", type=str, help="Output PSRFITS file")
    parser.add_argument(
        "-s", "--start", type=int, required=True, help="Start subint index, INclusive"
    )
    parser.add_argument(
        "-e", "--end", type=int, required=True, help="End subint index, EXclusive"
    )
    parser.add_argument(
        "--overwrite", action="store_true", help="If specified, overwrite output file"
    )
    return parser


def main():
    logging.basicConfig(
        level="DEBUG",
        format="%(asctime)s  %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s",
    )
    args = get_parser().parse_args()
    extract_subint_range(
        args.infile, args.outfile, args.start, args.end,
        overwrite=args.overwrite,
    )


if __name__ == "__main__":
    main()
