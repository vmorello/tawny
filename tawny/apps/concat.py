import logging
import argparse
from tawny import concatenate


def get_parser():
    parser = argparse.ArgumentParser(
        description='Concatenate contiguous PSRFITS files into a single one'
    )
    parser.add_argument('-o', '--outfile', type=str, required=True, help='Output PSRFITS file')
    parser.add_argument('--overwrite', action='store_true', 
        help='If specified, overwrite output file')
    parser.add_argument('infiles', type=str, nargs='+', help='Input PSRFITS files')
    return parser


def main():
    logging.basicConfig(
        level='DEBUG',
        format='%(asctime)s  %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s'
    )
    args = get_parser().parse_args()
    concatenate(args.infiles, args.outfile, overwrite=args.overwrite)


if __name__ == '__main__':
    main()
