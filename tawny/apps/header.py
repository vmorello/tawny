import argparse
from tawny import PSRFITS


def get_parser():
    parser = argparse.ArgumentParser(
        description='Print PSRFITS header'
    )
    parser.add_argument('fname', type=str, help='PSRFITS file')
    return parser


def main():
    args = get_parser().parse_args()
    pf = PSRFITS(args.fname)
    lines = [
        '',
        'Primary HDU',
        '===========',
        repr(pf.hprim.header),
        '',
        'Subint HDU',
        '==========',
        repr(pf.hsub.header).rstrip()
    ]
    print('\n'.join(lines) + '\n')


if __name__ == '__main__':
    main()
