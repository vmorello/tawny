import os
import astropy.io.fits as fits
from astropy.time import Time


# All FITS files must have a size that is a multiple of 2880 bytes
# This sometimes requires zero-padding at the end
FITS_BLOCK_SIZE = 2880

DAY_SECONDS = 86400.0


class PSRFITS(object):
    """ 
    Wrapper for an HDUList object returned by astropy's fits.open()
    
    Parameters
    ----------
    fname : str
        File name
    """
    def __init__(self, fname):
        self.fname = os.path.realpath(fname)
        self.hdu_list = fits.open(fname, mode='readonly', memmap=True)
        self.hprim = self.hdu_list['PRIMARY']
        self.hsub = self.hdu_list['SUBINT']

    @property
    def freqs(self):
        return self.hsub.data['DAT_FREQ'][0]

    @property
    def npol(self):
        return self.hsub.header['NPOL']

    @property
    def nchan(self):
        return self.hsub.header['NCHAN']

    @property
    def cbw(self):
        """ Channel bandwidth """
        return self.hsub.header['CHAN_BW']

    @property
    def nsub(self):
        return self.hsub.header['NAXIS2']

    @property
    def tsub(self):
        h = self.hsub.header
        return h['NSBLK'] * h['TBIN']

    @property
    def subint_bytesize(self):
        h = self.hsub.header
        return h['NAXIS1']

    def subint_byteoffset(self, isub):
        """ Byte offset of subint index isub from start of file """
        return self.subint_bytesize * isub + self.hsub._data_offset

    @property
    def mjd_start(self):
        h = self.hprim.header
        day = h['STT_IMJD']
        offset = (h['STT_SMJD'] + h['STT_OFFS']) / DAY_SECONDS
        return Time(day, offset, format='mjd')
