#
# VERY hacky script to generate prepsubband jobs for Mercer 5 processing
#
import os
from jinja2 import Template, StrictUndefined


def load_prepsubband_template():
    thisdir = os.path.dirname(__file__)
    path = os.path.join(thisdir, 'prepsubband.jinja2')
    with open(path, 'r') as f:
        template = Template(f.read(), undefined=StrictUndefined, trim_blocks=True)
    return template


PREPSUBBAND_TEMPLATE = load_prepsubband_template()


def generate_command(lodm, dmstep, numdms, nsub):
    # NOTE: make sure to apply the weights
    items = [
        'time prepsubband',
        '-lodm {:.4f}'.format(lodm),
        '-dmstep {:.4f}'.format(dmstep),
        '-numdms {:d}'.format(numdms),
        '-nsub {:d}'.format(nsub),
        '-psrfits',
        '-mask $MASKFILE',
        '-ignorechan $IGNORECHAN',
        '-noscales',
        '-nooffsets',
        '-o F1728_AM150',
        '$FILES'
    ]
    return ' '.join(items)


if __name__ == '__main__':
    start = 300.0
    end = 1200.0
    nsub = 128
    dmstep = 0.25
    numdms = 80
    cluster_dm = 650.0

    # dSubDM = 20.0
    callstep = numdms * dmstep
    lodm = start

    commands = {}
    while lodm < end:
        cmd = generate_command(lodm, dmstep, numdms, nsub)
        commands[lodm] = cmd
        lodm += callstep

    # Schedule commands in order of trial DM difference from cluster DM
    commands = [
        commands[lodm]
        for lodm in sorted(commands.keys(), key=lambda dm: abs(dm - cluster_dm))
    ]


    text = PREPSUBBAND_TEMPLATE.render(
        logsdir='/fred/oz002/vmorello/Mercer5/20201001/logs',
        workdir='/fred/oz002/vmorello/Mercer5/20201001/dedisp',
        commands=commands
    )
    print(text)