import os
from jinja2 import Template, StrictUndefined


def load_arrayjob_template():
    thisdir = os.path.dirname(__file__)
    path = os.path.join(thisdir, 'arrayjob.jinja2')
    with open(path, 'r') as f:
        template = Template(f.read(), undefined=StrictUndefined, trim_blocks=True)
    return template


ARRAYJOB = load_arrayjob_template()


def generate_arrayjob(**kwargs):
    return ARRAYJOB.render(**kwargs)


def test_dspsr():
    prologue = [
        'set -x',
        'BINS=128',
        'PERIOD=0.05119473248352126',
        'cd /fred/oz002/vmorello/Mercer5/20201001/tests'
    ]

    commands = [
        f'dspsr -A -U 884 -b $BINS -O subint_{ii:03d} -c $PERIOD ../rawdata/F1728_AM150_{ii}.sf'
        for ii in range(2)
    ]

    job = generate_arrayjob(
        name='test',
        prologue=prologue,
        commands=commands,
        logsdir='/fred/oz002/vmorello/Mercer5/20201001/tests',
        cpus=1,
        time='1:30:00',
        mem=4096,
        email='vmorello@gmail.com'
    )

    return job


if __name__ == '__main__':
    job = test_dspsr()
    print(job)
