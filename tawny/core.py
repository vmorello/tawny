import os
import logging
import subprocess
from copy import deepcopy
from collections import OrderedDict, namedtuple

import astropy.io.fits as fits
from astropy.time import Time
from tawny import PSRFITS, DAY_SECONDS, FITS_BLOCK_SIZE


log = logging.getLogger('tawny')


def extract_channel_range(infile, outfile, imin, imax, overwrite=False, reweight=True, 
    spectral_index=0.0):
    """
    Extract channel range imin:imax from PSRFITS file, and write the output to another PSRFITS 
    file.

    Parameters
    ----------
    infile : str
        Input file path
    outfile : str
        Output file path
    imin : int
        Index of the first channel to extract (INclusive)
    imax : int
        Index of the last channel to extract (EXclusive)
    overwrite : bool
        If True, overwrite 'outfile' if it already exists, raise an exception otherwise
    reweight : bool
        If True, set the weights the channel weights to the optimal value when searching for
        sources with known spectral index alpha. 
        weight = (f / f0) ** alpha
    spectral_index : float
        Source spectral index postulated if re-weighting.
    """
    infile = os.path.realpath(infile)
    outfile = os.path.realpath(outfile)

    if os.path.exists(outfile) and not overwrite:
        raise OSError("File already exists: {!r}".format(outfile))

    log.info('Processing PSRFITS file {!r}'.format(infile))
    pf = PSRFITS(infile)

    if not (0 <= imin < pf.nchan):
        raise ValueError("imin out of acceptable range [0, {}]".format(pf.nchan - 1))
    if not (0 < imax <= pf.nchan):
        raise ValueError("imax out of acceptable range [0, {}]".format(pf.nchan))
    if not imax > imin:
        raise ValueError("imax must be > imin")

    ### Construct output HDUs ###
    ### Primary HDU
    hprim = fits.PrimaryHDU(header=deepcopy(pf.hprim.header))
    new_cfreq = (pf.freqs[imin] + pf.freqs[imax - 1]) / 2.0
    new_nchan = imax - imin
    new_bw = pf.cbw * new_nchan

    log.debug('Extracting channel range {}:{}'.format(imin, imax))
    log.debug('New fmin = {}'.format(pf.freqs[imin] - pf.cbw/2))
    log.debug('New fmax = {}'.format(pf.freqs[imax - 1] + pf.cbw/2))

    log.debug('New OBSFREQ = {}'.format(new_cfreq))
    hprim.header.set('OBSFREQ', new_cfreq)

    log.debug('New OBSBW = {}'.format(new_bw))
    hprim.header.set('OBSBW', new_bw)

    log.debug('New OBSNCHAN = {}'.format(new_nchan))
    hprim.header.set('OBSNCHAN', new_nchan)

    ### Subint HDU
    # The keywords defining the table structure are automatically set when manually creating
    # the table data (TFORM*, TDIM*, etc.)

    # Create data columns in the SAME ORDER
    columns_dict = OrderedDict()

    for col_params in pf.hsub.columns:
        name = col_params.name
        data = pf.hsub.data[name]

        # Default arguments to fits.Column() constructor: copy the original column parameters
        kwargs = dict(
            name = col_params.name,
            format = col_params.format,
            unit = col_params.unit,
            dim = col_params.dim,
            array = data
        )

        # In these columns the data are 1-dimensional, with NCHAN elements
        if name in ('DAT_FREQ', 'DAT_WTS', 'DAT_OFFS', 'DAT_SCL'):
            # shape = (nsub, nchan)
            kwargs['array'] = data[:, imin:imax]
            kwargs['format'] = '{}D'.format(new_nchan) # D = double

        elif name == 'DATA':
            # shape = (nsub, nsblk / (8 / nbit), npol, nchan)
            array = data[:, :, :, imin:imax]
            block_rows = array.shape[1]
            kwargs['array'] = array
            kwargs['format'] = '{}B'.format(new_nchan * block_rows * pf.npol) # B = bytes

            # 'dim' string wants FORTRAN-style array dimension (reversed from numpy order)
            kwargs['dim'] = '({},{},{})'.format(new_nchan, pf.npol, block_rows)

        column = fits.Column(**kwargs)
        columns_dict[name] = column

    columns = fits.ColDefs(list(columns_dict.values()))

    # NOTE: keywords specifically related to defining the table structure (such as the "TXXXn" 
    # keywords like TTYPEn) are automatically overridden by the supplied column definitions
    hsub = fits.BinTableHDU.from_columns(columns, header=pf.hsub.header)
    hsub.header.set('NCHAN', new_nchan)

    ### Re-weight
    if reweight:
        log.info('Re-weighting channels with spectral index = {:.2f}'.format(spectral_index))
        freqs = pf.freqs[imin:imax]
        weights = (freqs / freqs[0]) ** spectral_index
        hsub.data['DAT_WTS'] = weights

    hdu_list = fits.HDUList([hprim, hsub])
    hdu_list.writeto(outfile, overwrite=overwrite)
    log.info('Saved new PSRFITS to {!r}'.format(outfile))


def convert_time(time):
    """
    Convert astropy Time to PSRFITS MJD format (mjd_days, seconds, fractional_second)

    Parameters
    ----------
    time : astropy.time.Time

    Returns
    -------
    psrfits_time : dict
        Dictionary with keys 
            'STT_IMJD' : Number of days (int)
            'STT_SMJD' : Integer seconds from 00h UTC (int)
            'STT_OFFS' : Fractional seconds from STT_SMJD (float)
    """
    mjd = time.mjd 
    days = int(mjd)
    delta = time - Time(days, format='mjd') # let astropy do the high-precision diff
    seconds = delta.value * DAY_SECONDS
    offset = seconds - int(seconds)
    offset = int(offset * 1.0e9) / 1.0e9 # round to nearest nanosecond
    return {
        'STT_IMJD' : days, 
        'STT_SMJD' : int(seconds), 
        'STT_OFFS' : offset
    }


def extract_subint_range(infile, outfile, imin, imax, overwrite=False):
    """
    Extract sub-integration range imin:imax from PSRFITS file, and write the output to another 
    PSRFITS file.

    Parameters
    ----------
    infile : str
        Input file path
    outfile : str
        Output file path
    imin : int
        Index of the first subint to extract (INclusive)
    imax : int
        Index of the last subint to extract (EXclusive)
    overwrite : bool
        If True, overwrite 'outfile' if it already exists, raise an exception otherwise
    """
    infile = os.path.realpath(infile)
    outfile = os.path.realpath(outfile)

    if os.path.exists(outfile) and not overwrite:
        raise OSError("File already exists: {!r}".format(outfile))

    log.info('Processing PSRFITS file {!r}'.format(infile))
    pf = PSRFITS(infile)

    if not (0 <= imin < pf.nsub):
        raise ValueError("imin out of acceptable range [0, {}]".format(pf.nsub - 1))
    if not (0 < imax <= pf.nsub):
        raise ValueError("imax out of acceptable range [0, {}]".format(pf.nsub))
    if not imax > imin:
        raise ValueError("imax must be > imin")

    nsub_new = imax - imin
    tobs_new = nsub_new * pf.tsub

    ### New primary header
    hprim = fits.PrimaryHDU(header=deepcopy(pf.hprim.header))
    time_params = convert_time(pf.mjd_start + (imin * pf.tsub) / DAY_SECONDS)
    log.debug("Time offset (seconds): {}".format(pf.tsub * imin))
    log.debug("New start time: {}".format(time_params))
    hprim.header.update(time_params)

    ### Subint HDU
    # Create new data columns in the same order but without data
    columns = [
        fits.Column(name=col.name, format=col.format, unit=col.unit, dim=col.dim, array=None)
        for col in pf.hsub.columns
    ]
    hsub = fits.BinTableHDU.from_columns(columns, header=deepcopy(pf.hsub.header))
    hsub.header['NSTOT'] = hsub.header['NSBLK'] * nsub_new

    hdu_list = fits.HDUList([hprim, hsub])
    hdu_list.writeto(outfile, overwrite=overwrite)
    log.debug('Created new PSRFITS: {!r}'.format(outfile))

    # Now we have written an empty PSRFITS file, we need to:
    # 1. Hack the NAXIS2 value, because astropy set it to zero above
    log.debug("Updating NAXIS2 = {}".format(nsub_new))
    f = fits.open(outfile, mode='update')
    hsub = f['SUBINT']
    hsub.header['NAXIS2'] = nsub_new
    f.flush()
    f.close()

    # 2. Append the correct range of subint data using shell commands
    log.info("Appending sub-integrations data to output file")
    log.info(f"Total data time span to append (seconds): {tobs_new:.2f}")

    for isub in range(imin, imax):
        offset = pf.subint_byteoffset(isub)
        size = pf.subint_bytesize
        cmd = f"tail -c +{offset+1} {infile} | head -c {size} >> {outfile}"
        log.debug(f"Appending subint index {isub}")
        log.debug(cmd)
        subprocess.call(cmd, shell=True)

    log.debug("Zero-padding to multiple of FITS_BLOCK_SIZE")
    pad_bytes = FITS_BLOCK_SIZE - os.stat(outfile).st_size % FITS_BLOCK_SIZE
    cmd = f"dd if=/dev/zero bs=1 count={pad_bytes} >> {outfile}"
    log.debug(cmd)
    subprocess.call(cmd, shell=True)
    log.info("Done.")


def concatenate(infiles, outfile, overwrite=False):
    """
    Concatenate a list of PSRFITS files. 
    WARNING: This function does NOT check whether the operation is sensible.

    Parameters
    ----------
    infiles : list of str
        Input file paths, in any order. They will be sorted in MJD Start order.
    outfile : str
        Output file path
    overwrite : bool
        If True, overwrite 'outfile' if it already exists, raise an exception otherwise
    """
    if os.path.exists(outfile) and not overwrite:
        raise OSError("File already exists: {!r}".format(outfile))

    ### Sort files in time order
    log.info("Reading all input PSRFITS headers")
    FileParams = namedtuple('FileParams', 'fname mjd_start nsub tobs')
    fparams = []
    for infile in infiles:
        pf = PSRFITS(infile)
        par = FileParams(
            fname=os.path.realpath(infile),
            mjd_start=pf.mjd_start,
            nsub=pf.nsub,
            tobs=pf.nsub * pf.tsub
        )
        fparams.append(par)
    
    fparams = sorted(fparams, key=lambda par: par.mjd_start)
    infiles = [par.fname for par in fparams]

    log.info("Files will be concatenated in the following order")
    for infile in infiles:
        log.debug(infile)

    pfstart = PSRFITS(infiles[0])

    # Primary header
    hprim = fits.PrimaryHDU(header=deepcopy(pfstart.hprim.header))

    # Subint HDU
    # Create new data columns in the same order but without data
    columns = [
        fits.Column(name=col.name, format=col.format, unit=col.unit, dim=col.dim, array=None)
        for col in pfstart.hsub.columns
    ]
    hsub = fits.BinTableHDU.from_columns(columns, header=deepcopy(pfstart.hsub.header))

    hdu_list = fits.HDUList([hprim, hsub])
    hdu_list.writeto(outfile, overwrite=overwrite)
    log.info('Created new PSRFITS: {!r}'.format(outfile))

    # Re-load output file and hack NAXIS2
    # Total number of subints
    nsub_total = sum((
        PSRFITS(fname).nsub
        for fname in infiles
    ))
    log.debug("Updating NAXIS2 = {}".format(nsub_total))
    f = fits.open(outfile, mode='update')
    hsub = f['SUBINT']
    hsub.header['NAXIS2'] = nsub_total
    hsub.header['NSTOT'] = nsub_total * hsub.header['NSBLK']
    f.flush()
    f.close()

    # Append data using shell commands
    for infile in infiles:
        pf = PSRFITS(infile)
        offset = pf.subint_byteoffset(0)
        size = pf.subint_bytesize * pf.nsub
        cmd = f"tail -c +{offset+1} {infile} | head -c {size} >> {outfile}"
        log.debug(f"Appending subint data from {infile!r}")
        log.debug(cmd)
        subprocess.call(cmd, shell=True)

    # Zero-pad if need be
    outfile_size = os.stat(outfile).st_size
    if outfile_size % FITS_BLOCK_SIZE:
        log.debug("Zero-padding to multiple of FITS_BLOCK_SIZE")
        pad_bytes = FITS_BLOCK_SIZE - outfile_size % FITS_BLOCK_SIZE
        cmd = f"dd if=/dev/zero bs=1 count={pad_bytes} >> {outfile}"
        log.debug(cmd)
        subprocess.call(cmd, shell=True)
        log.info("Done.")



if __name__ == '__main__':
    import coloredlogs
    coloredlogs.install(level='DEBUG')

    infiles = [
        '/home/vince/work/M5/first2subs.sf',
        '/home/vince/work/M5/first2subs_2.sf',
    ]
    outfile = '/home/vince/work/M5/test.sf'

    concatenate(infiles, outfile, overwrite=True)
    