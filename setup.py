import os
import setuptools
import subprocess
from setuptools import setup


VERSION = '0.0.1'


with open("README.md", "r") as fh:
    long_description = fh.read()


install_requires = [
    'astropy',
]


setup(
    name='tawny',
    url='https://bitbucket.org/vmorello/tawny',
    author='Vincent Morello',
    author_email='vmorello@gmail.com',
    description='Manipulate Parkes UWL PSRFITS files',
    long_description=long_description,
    long_description_content_type='text/markdown',
    version=VERSION,
    packages=setuptools.find_packages(),
    install_requires=install_requires,
    license='MIT License',

    entry_points = {
        'console_scripts': [
            'uwl_header=tawny.apps.header:main',
            'uwl_xcr=tawny.apps.xcr:main',
            'uwl_xsr=tawny.apps.xsr:main',
            'uwl_concat=tawny.apps.concat:main',
            'uwl_xcr_bulk=tawny.apps.xcr_bulk:main',
        ],
    },

    # NOTE (IMPORTANT): This means that everything mentioned in MANIFEST.in will be copied at install time 
    # to the packages folder placed in 'site-packages'
    include_package_data=True,

    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Unix",
        "Topic :: Scientific/Engineering :: Astronomy"
        ],
)
